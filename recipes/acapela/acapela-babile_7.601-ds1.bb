# vim: set ft=sh ts=8 sw=8 noet:
DESCRIPTION = "Acapela Mobility Babile"
LICENSE = "Proprietary"

SRC_URI = "http://somewhere/${PN}-${PV}.tar.bz2"

# library requires CP1252 encoded data
RDEPENDS += "eglibc-gconv-cp1252"

PR="r1"

do_configure() {
	local libs=

	case "${BASE_PACKAGE_ARCH}" in
	armv5*|armv7a)
		libs="linux arm gcc 4.4"
		;;
	*)
		echo "${BASE_PACKAGE_ARCH}: architecture not supported" >&2
		exit 1
		;;
	esac

	libs="${S}/lib/$libs"

	ln -sn "$libs" "${WORKDIR}/libs"
}

do_compile() {
	local cflags="-DPLATFORM_LINUX"
	local d='$'

	case "${BASE_PACKAGE_ARCH}" in
	armv5*) cflags="-DCPU_RISC_ARM7 $cflags" ;;
	esac

	cat <<EOT > ${PN}.pc
prefix=${prefix}
exec_dir=$d{prefix}
libdir=${libdir}
includedir=${includedir}/babile

Name: ${PN}
Description: Acapela Mobility Babile
Version: ${PV}
Libs: -L$d{libdir} -lbabile
Cflags: -I$d{includedir} $cflags
EOT
}

do_install() {
	install -d ${D}${docdir}/babile
	install -d ${D}${includedir}/babile
	install -d ${D}${libdir}/pkgconfig

	install -m 0644 ${S}/Documentation/* ${D}${docdir}/babile/
	install -m 0644 ${S}/include/*.h ${D}${includedir}/babile/
	install -m 0644 ${WORKDIR}/libs/*/*babile* ${D}${libdir}/
	install -m 0644 ${PN}.pc ${D}${libdir}/pkgconfig
}

FILES_${PN}-dev  = "${includedir}"
FILES_${PN}-dev += "${libdir}/pkgconfig"
FILES_${PN}     += "${libdir}/*.so*"

# ERROR: QA Issue with acapela-babile: No GNU_HASH in the elf binary: '...../acapela-babile/usr/lib/libbabile.so'
INSANE_SKIP_${PN} = True

SRC_URI[md5sum] = "9b0e6e72bcd17c52b4fcfaa18d122289"
SRC_URI[sha256sum] = "a074e8e14d20ca57462072b0154decd8f8915bc5b258c1e2558b8ba2fa986f45"
