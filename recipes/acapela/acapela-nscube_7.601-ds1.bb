# vim: set ft=sh ts=8 sw=8 noet:
DESCRIPTION = "Acapela Mobility Nscapi"
LICENSE = "Proprietary"

SRC_URI = "http://somewhere/${PN}-${PV}.tar.bz2"

PR="r0"
RDEPENDS = "acapela-babile"

do_configure() {
	local libs=

	case "${BASE_PACKAGE_ARCH}" in
	armv5*) libs="linux arm gcc 4.4" ;;
	esac

	[ -z "$libs" ] || libs="${S}/lib/$libs"

	ln -sn "$libs" "${WORKDIR}/libs"
}

do_compile() {
	local cflags="-DPLATFORM_LINUX"
	local d='$'

	case "${BASE_PACKAGE_ARCH}" in
	armv5*) cflags="-DCPU_RISC_ARM7 $cflags" ;;
	esac

	cat <<EOT > ${PN}.pc
prefix=${prefix}
exec_dir=$d{prefix}
libdir=${libdir}
includedir=${includedir}/babile

Name: ${PN}
Description: Acapela Mobility Nscapi
Version: ${PV}
Libs: -L$d{libdir} -lnscube -lbabile
Cflags: -I$d{includedir} $cflags
EOT
}

do_install() {
	install -d ${D}${docdir}/babile
	install -d ${D}${includedir}/babile
	install -d ${D}${libdir}/pkgconfig

	install -m 0644 ${S}/Documentation/* ${D}${docdir}/babile/
	install -m 0644 ${S}/include/*.h ${D}${includedir}/babile/
	install -m 0644 ${WORKDIR}/libs/*/*nscube* ${D}${libdir}/
	install -m 0644 ${PN}.pc ${D}${libdir}/pkgconfig
}

FILES_${PN}-dev  = "${includedir}"
FILES_${PN}-dev += "${libdir}/pkgconfig"
FILES_${PN}     += "${libdir}/*.so*"

# ERROR: QA Issue with acapela-babile: No GNU_HASH in the elf binary: '...../acapela-babile/usr/lib/libbabile.so'
INSANE_SKIP_${PN} = True

SRC_URI[md5sum] = "2facb4df6858437f8cd076717c551767"
SRC_URI[sha256sum] = "32994b2d2a0354c5c8254d27a78be98e527c34faba9e534ce964f2b1eefc732d"
