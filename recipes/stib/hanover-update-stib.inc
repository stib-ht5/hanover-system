# vim: set ft=sh ts=4 sw=4 et:
DESCRIPTION = "Hanover system software update utilities package"
HOMEPAGE = "http://ebs/redmine/projects"
LICENSE = "MIT"

inherit distutils

INC_PR = "r4"
PR = "${INC_PR}"
PE = "0"

BRANCH = "master"
SRCREV = "${PV}"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/stib/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v2.0.5.md5sum] = "25e548803352f9e15582a9d53b177c74"
SRC_URI[v2.0.5.sha256sum] = "d56813bcd476d289d554ffcc9d6e46af03e584016eceb520d9ef2feb7f9dea81"

S = "${WORKDIR}/git"

RDEPENDS_${PN} += "hanover-update"
RDEPENDS_${PN} += "python-hanover"
RDEPENDS_${PN} += "firmloader-signs"

# runit
DISTUTILS_INSTALL_ARGS += "--runitdir=${D}${sysconfdir}/sv"
FILES_${PN} += "${sysconfdir}/sv"

# update.sh
do_install_append() {
	mkdir -p ${D}${sysconfdir}/init.d
	install -m 0755 ${S}/update.sh ${D}${sysconfdir}/init.d/
	echo "rm -f ${sysconfdir}/setup.d/update" >> ${D}${sysconfdir}/init.d/update.sh
}

pkg_postinst_${PN}_append() {
	if [ -L /var/service/updated ]; then
		mkdir -p ${sysconfdir}/setup.d
		ln -s ../init.d/update.sh ${sysconfdir}/setup.d/update
	fi
}

FILES_${PN} += "${sysconfdir}/init.d/"
