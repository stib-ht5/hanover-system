require u-boot-davinci.inc


BRANCH = "psp.03.21.00.03"
SRCREV = "U-BOOT-HANOVER-03.21.00.04"

do_deploy_append () {
 install -m 0775 ${S}/tools/uflash/uflash ${DEPLOY_DIR_IMAGE}
 install -m 0644 ${S}/tools/uflash/README ${DEPLOY_DIR_IMAGE}/uflash.README
 install -m 0775 ${S}/tools/mkimage       ${DEPLOY_DIR_IMAGE}
 install -m 0775 ${WORKDIR}/mksdboot.sh   ${DEPLOY_DIR_IMAGE}
 install -m 0775 ${WORKDIR}/boot.cmd      ${DEPLOY_DIR_IMAGE}
}
