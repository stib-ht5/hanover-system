# Using relative path here causes bitbake to search in
# BBPATH for the first instance of u-boot.inc rather
# than just within the current directory.
require recipes/u-boot/u-boot.inc

DESCRIPTION = "u-boot bootloader for Hanover HTC SD boot"

COMPATIBLE_MACHINE = "dm365-htc"

UBOOT_DIR = "${OEBASE}/hanover-system/recipes/u-boot"


SRC_URI = "git://${HANOVER_GIT_LEGACY}/bsp/u-boot-davinci.git;protocol=git;branch=${BRANCH} \
           file://mksdboot.sh  \
           file://boot.cmd     \
           file://update.cmd   \
"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"

DEPENDS += "ubl mtd-utils"

do_compile_prepend () {
    
    oe_runmake tools clean
    
    #Build mkimage for the target platform
    oe_runmake HOSTCC="${TARGET_PREFIX}gcc" HOSTSTRIP="${TARGET_PREFIX}strip" tools env  
    cp tools/mkimage tools/mkimage-${TARGET_ARCH}
    cp tools/env/fw_printenv tools/env/fw_printenv-${TARGET_ARCH}
    # Clean after us, to prepare for the default build
    oe_runmake HOSTCC="${TARGET_PREFIX}gcc" HOSTSTRIP="${TARGET_PREFIX}strip" tools clean  
}

do_install_append () {
    install -d ${D}/boot
    install -d ${D}${base_sbindir}
    install -d ${D}${sysconfdir}

    install -m 0755 ${UBOOT_DIR}/${MACHINE}/update.cmd         ${D}/boot/
    #mkimage -A arm -O linux -T script -C none -a 0 -e 0 -n 'Execute uImage' -d ${D}/boot/update.cmd ${D}/boot/disable.update.scr
   
    install -m 755 ${S}/tools/env/fw_printenv-${TARGET_ARCH} ${D}${base_sbindir}/fw_printenv
    #install -m 755 ${S}/tools/env/fw_printenv ${D}${base_sbindir}/fw_setenv
    ln -sf  fw_setenv ${D}${base_sbindir}/fw_printenv
    install -m 644 ${UBOOT_DIR}/${MACHINE}/fw_env.config ${D}${sysconfdir}/ 
}


PACKAGES += "${PN}-fw-utils"
FILES_${PN}-fw-utils = "${base_sbindir}/fw_printenv" 
FILES_${PN}-fw-utils += "${base_sbindir}/fw_setenv"
FILES_${PN}-fw-utils += "${sysconfdir}/fw_env.config"


# u-boot doesn't use LDFLAGS for fw files, needs to get fixed, but until then:
INSANE_SKIP_${PN}-fw-utils = True

CONFFILES_${PN}-fw-utils =             "\
  ${sysconfdir}/fw_env.config           \
"
