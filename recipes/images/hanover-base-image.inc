inherit image

require ${OEBASE}/arago/recipes/images/arago-image.inc

INC_PR = "r3"

PACKAGE_ARCH = "${MACHINE_ARCH}"

IMAGE_PREPROCESS_COMMAND += "; install -m 0644 ${IMAGE_ROOTFS}/usr/lib/opkg/status \
	${DEPLOY_DIR_IPK}/${MACHINE}/opkg.status"

IMAGE_INSTALL = "\
	task-hanover-base	\
	"

# release and mksdimage scripts
DEPENDS += "build-scripts"

# allow images to inject their own release scripts
do_deploy() {
	install -d ${DEPLOY_DIR_IMAGE}/scripts/
}

do_deploy[dirs] = "${WORKDIR}"
addtask deploy before do_package_stage after do_install
