require hanover-base-image.inc

PV = "${HANOVER_VERSION}"
PR = "${INC_PR}"

export IMAGE_BASENAME = "${PN}_${PV}"
