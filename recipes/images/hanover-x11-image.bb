require hanover-base-image.inc

PV = "${HANOVER_VERSION}"
PR = "${INC_PR}"

XSERVER ?= "xserver-kdrive-fbdev"
export IMAGE_BASENAME = "${PN}_${PV}"

IMAGE_INSTALL += "\
	${XSERVER} \
	task-base-extended \
	angstrom-x11-base-depends \
        "
