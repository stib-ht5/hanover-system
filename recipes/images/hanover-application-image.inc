require hanover-base-image.inc

PV = "${PRODUCT_RELEASE}"
PR = "${INC_PR}"

PROVIDES += "hanover-image"

IMAGE_INSTALL += "\
	task-hanover-${PRODUCT}		\
        "

export IMAGE_BASENAME = "${PRODUCT}-image.${PV}"
