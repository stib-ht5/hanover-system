require recipes/tzcode/tzcode-native.inc

TZDATA_PV = "2018e"

SRC_URI[tzcode-2018e.md5sum] = "c4d7df0fff7ba5588b32c5f27e2caf97"
SRC_URI[tzcode-2018e.sha256sum] = "ca340cf20e80b699d6e5c49b4ba47361b3aa681f06f38a0c88a8e8308c00ebce"
SRC_URI[tzdata-2018e.md5sum] = "97d654f4d7253173b3eeb76a836dd65e"
SRC_URI[tzdata-2018e.sha256sum] = "6b288e5926841a4cb490909fe822d85c36ae75538ad69baf20da9628b63b692e"

S = "${WORKDIR}"

do_compile() {
	oe_runmake zic zdump tzselect
}
