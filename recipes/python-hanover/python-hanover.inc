DESCRIPTION = "Python Hanover library"
SECTION = "devel/python"
HMEPAGE = "http://www.hanoverdisplays.com"
LICENSE = "GPL"
PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.0.3.md5sum] = "f846a1d9eb2e847f8f199b2e06f9c9f1"
SRC_URI[v1.0.3.sha256sum] = "2d1b8341b880aadc81061bfe5f3927f66f80c20d0c865a7adc27f46b0f3c28c7"

SRCREV = "${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

inherit distutils
