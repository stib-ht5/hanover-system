#!/bin/sh

set -u

MEDIA_BASE=/media/removable

log_info() {
	logger -t automount -p user.info "$@"
}

log_err() {
	logger -t automount -p user.err "$@"
}

rm_symlinks() {
	local d= m0= m1=

	for d in $MEDIA_BASE*; do
		if [ -L "$d" ]; then
			m0="$(readlink -f "$d")"

			if [ ! -d "$d/" ]; then
				log_info "$d: removing dangling symlink"
				rm -f "$d"
			elif ! grep -q " $m0 " /proc/mounts; then
				log_info "$d: $m0 not a mount point, removing."
				rm -f "$d"
			else
				for m1; do
					if [ "$m0" = "$m1" ]; then
						log_info "$d: removing becase $m1 is going to be removed"
						rm -f "$d"
					fi
				done
			fi

		fi
	done
}

add_symlink() {
	local i=1 m="$1" errno=0

	# remove dangling symlinks
	rm_symlinks

	if ! grep -q " $m " /proc/mounts; then
		log_err "$m: is not mounted"
		exit 1
	fi

	# attempt to reuse from a previous life
	for d in $MEDIA_BASE*; do
		if [ -L "$d" -a "$(readlink "$d")" = "$m" ]; then
			log_info "$d: symlink to $m reused"
			exit 0
		fi
	done

	# attempt to create symlink
	while ! ln -sn "$m" $MEDIA_BASE$i 2> /dev/null; do
		i=$(expr $i + 1)

		# panic exit
		if [ $i -eq 100 ]; then
			log_err "$m: giving up attempting to create a $MEDIA_BASE* symlink"
			exit 1
		fi
	done
	log_info "$MEDIA_BASE$i: created as symlink to $m"
}

case "${1:-}" in
add)
	if [ -d "${2:-}" ]; then
		add_symlink "$2"
	fi
	;;
remove)
	shift
	rm_symlinks "$@"
	;;
*)
	rm_symlinks
	;;
esac
