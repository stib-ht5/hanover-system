PR_append = "-hdl2"

SRC_URI += "file://removable_symlink.sh"

do_install_append() {
	install -m 0755 "${WORKDIR}/removable_symlink.sh" "${D}/${sysconfdir}/udev/scripts/"
	install -d "${D}/${sysconfdir}/udev/removable.d"
	ln -sf ../scripts/removable_symlink.sh "${D}/${sysconfdir}/udev/removable.d/symlink.sh"
}
