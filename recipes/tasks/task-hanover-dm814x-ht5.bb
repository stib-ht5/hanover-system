################################################################################
# Task, for building basic packages which are used currently in HTC.
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
#
# Copyright(C) 2012 Hanover Displays Ltd.
# This file is licensed under the terms of the GNU General Public License
# version 2. This program  is licensed "as is" without any warranty of any kind,
# whether express or implied.
################################################################################
DESCRIPTION = "TI DM814x base packages"
LICENSE = "MIT"
PV = "${HANOVER_VERSION}"
PR = "r13"

inherit task

#kernel-module-iptable-nat 
#kernel-module-iptable-mangle 
#kernel-module-ipt-masquerade 

COMPATIBLE_MACHINE = "dm814x-ht5"
PACKAGE_ARCH = "${MACHINE_ARCH}"
	
# those ones can be set in machine config to supply packages needed to get machine booting
MACHINE_ESSENTIAL_EXTRA_RDEPENDS = "\
"
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS = "\
"

TI_DM814X_STIB =              "\
  gst-openmax-ti             \
  ti-firmware                \
  ti-cmem-module             \
  ti-syslink                 \
  ti-syslink-module          \ 
  ti-media-controller-utils  \
  u-boot-omap3-ht5           \
  u-boot-omap3-ht5-fw-utils  \
  kernel                     \
  kernel-image               \
  kernel-modules             \
  omap3-sgx-modules          \
  hdl-ht5-firmware           \
  "
# ti-media-controller-loader
     
RDEPENDS_${PN} = "\
      ${TI_DM814X_STIB} \
      ${MACHINE_ESSENTIAL_EXTRA_RDEPENDS} \
    "

RRECOMMENDS_${PN} = "\
    ${MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS} \
    "

