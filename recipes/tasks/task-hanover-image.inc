# vim: set ft=sh noet ts=8 sw=8:
DESCRIPTION = "${PRODUCT} image task"
LICENSE = "MIT"

inherit task

INC_PR = "r1"
PR = "${INC_PR}"

RDEPENDS_${PN} = "\
	task-hanover-base		\
	hanover-configs-${PRODUCT}	\
	hanover-${PRODUCT}-version	\
	hanover-${PRODUCT}-setup	\
	"

# task-hanover-image
PROVIDES += "task-hanover-image"
RREPLACES_${PN} += "task-hanover-image"
