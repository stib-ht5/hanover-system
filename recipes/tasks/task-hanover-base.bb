DESCRIPTION = "Hanover base image task"
LICENSE = "MIT"
PV = "${HANOVER_VERSION}"
PR = "r9"

inherit task

PACKAGE_ARCH = "${MACHINE_ARCH}"

# as this is the common base we can't allow any dependency
# in packages depending in ${PRODUCT} specific information
# or it wouldn't be a ${PRODUCT}-agnostic base after all
#

export TASK_HANOVER_COMMON_RDEPENDS = "	\
	libltdl				\
	coreutils			\
	task-hanover-${MACHINE}		\
	hanover-security		\
	hanover-update			\
	hanover-version			\
	"

# hanover-products/${PRODUCT}/${RELDIR}/packages.inc

TASK_HANOVER_BASE = "\
	mtd-utils		\
	curl			\
	dcron			\
	logrotate		\
	arago-feed-configs	\
	devmem2			\
	ethtool			\
	tcpdump			\
	base-files		\
	base-passwd		\
	busybox			\
	htcdev			\
	initscripts		\
	modutils-initscripts	\
	netbase			\
	update-alternatives	\
	module-init-tools	\
	sysvinit-pidof		\
	lrzsz			\
	tzdata			\
	"
# initscript-telnetd    \

TASK_HANOVER_ALSA_UTILS = "\
	alsa-conf		\
	alsa-conf-base		\
	alsa-utils-alsamixer	\
	alsa-utils-aplay	\
	alsa-utils-amixer	\
	"

#    alsa-server \
#    alsa-state \
#    alsa-utils-midi \
#    alsa-utils-aconnect \
#    alsa-utils-iecset \
#    alsa-utils-speakertest \
#    alsa-utils-aseqnet \
#    alsa-utils-aseqdump \
#    alsa-utils-alsaconf \
#    alsa-utils-alsactl  \

TASK_HANOVER_UTILS = "\
	dropbear		\
	gdbserver		\
	i2c-tools		\
	fbset			\
	usbutils		\
	iproute2		\
	udev-utils		\
	nfs-utils		\
	nmap			\
	rsync			\
	wget			\
	bash			\
	lmsensors-sensors	\
	net-snmp-mibs		\
	net-snmp-server		\
	net-snmp-client		\
	tar			\
	bzip2			\
	netcat			\
	"

# fbset-modes            \

TASK_HANOVER_NTP = "\
	ntpdate			\
	"
#   ntp                    \
#   ntp-bin                \
#   ntp-tickadj            \
#   ntp-utils              \

TASK_HANOVER_EDITORS = "\
	vim			\
	nano			\
	"

TASK_HANOVER_JPEG_VIEWER = "\
	"

#  fbida                   \
#  tiff                    \

TASK_HANOVER_PYTHON = "\
python-audio \
python-avahi \
python-bsddb \
python-codecs \
python-compile \
python-compiler \
python-compression \
python-core \
python-crypt \
python-ctypes \
python-curses \
python-datetime \
python-db \
python-dbus \
python-debugger \
python-difflib \
python-distutils \
python-doctest \
python-elementtree \
python-email \
python-fcntl \
python-gdbm \
python-hotshot \
python-html \
python-idle \
python-image \
python-io \
python-json \
python-lang \
python-logging \
python-mailbox \
python-man \
python-math \
python-mime \
python-misc \
python-mmap \
python-modules \
python-multiprocessing \
python-netclient \
python-netserver \
python-numbers \
python-numpy \
python-pickle \
python-pkgutil \
python-pprint \
python-profile \
python-pydoc \
python-re \
python-readline \
python-resource \
python-robotparser \
python-shell \
python-smtpd \
python-sqlite3-tests \
python-sqlite3 \
python-stringold \
python-subprocess \
python-syslog \
python-terminal \
python-tests \
python-textutils \
python-threading \
python-unittest \
python-unixadmin \
python-xml \
python-xmlrpc \
python-zlib \
"
#python-mysqldb \
#python-tkinter \
#python-pycairo \
#python-pygobject \
#python-pygtk \

TASK_HANOVER_FSUTILS =   "\
	e2fsprogs		\
	e2fsprogs-e2fsck	\
	e2fsprogs-mke2fs	\
	e2fsprogs-tune2fs	\
	dosfstools		\
	mtd-utils		\
	"

TASK_HANOVER_GST = "\
   gstreamer \
   gst-plugins-base \
   gst-plugins-good \
   gst-plugins-bad \
   gst-plugins-ugly \
   gst-plugins-base-meta \
   gst-plugins-good-meta \
   gst-plugins-bad-meta \
   gst-plugins-ugly-meta \
   gst-rtsp \
"

TASK_HANOVER_NCURSES =   "\
    ncurses-terminfo      \
    ncurses-tools         \
    "

TASK_HANOVER_ROUTER = "\
	iptables		\
	"

TASK_HANOVER_APPS =      "\
	unittest-cpp            \
	"

export TASK_HANOVER_PRODUCT =       "\
	${TASK_HANOVER_BASE}          \
	${TASK_HANOVER_UTILS}         \
	${TASK_HANOVER_ALSA_UTILS}    \ 
	${TASK_HANOVER_EDITORS}       \
	${TASK_HANOVER_JPEG_VIEWER}   \
	${TASK_HANOVER_NTP}           \
	${TASK_HANOVER_FSUTILS}       \
	${TASK_HANOVER_GST}           \
	${TASK_HANOVER_NCURSES}       \
	${TASK_HANOVER_PYTHON}        \
	${TASK_HANOVER_APPS}          \
	${TASK_HANOVER_ROUTER}        \
	"

###############################################################################
# Machine specific preferred versions                                         #
###############################################################################

#TI DM814X packages
PREFERRED_VERSION_ti-media-controller-loader = "3.00.00.07"
PREFERRED_VERSION_ti-c674x-aaclcdec = "01.41.00.00"
PREFERRED_VERSION_ti-codec-engine = "3.22.01.06"
PREFERRED_VERSION_ti-edma3lld = "02.11.05.02"
PREFERRED_VERSION_ti-framework-components = "3.22.01.07"
PREFERRED_VERSION_ti-ipc = "1.24.03.32"
PREFERRED_VERSION_ti-linuxutils = "3.22.00.02"
PREFERRED_VERSION_ti-osal = "1.22.01.09"
PREFERRED_VERSION_ti-slog = "04.00.00.02"
PREFERRED_VERSION_ti-sysbios = "6.33.05.46"
PREFERRED_VERSION_ti-uia = "1.01.01.14"
PREFERRED_VERSION_ti-xdctools = "3.23.03.53"
PREFERRED_VERSION_ti-xdais = "7.22.00.03"
PREFERRED_VERSION_ti-rpe = "1.00.00.12"
PREFERRED_VERSION_ti-dspbios = "6.33.05.46"

#Upgraded 
PREFERRED_VERSION_gst-openmax-ti = "07.00.00"
PREFERRED_VERSION_ti-omx = "05.02.00.48"
PREFERRED_VERSION_ti-syslink = "2.20.02.20"
PREFERRED_VERSION_ti-cgt6x = "7.3.4"

#System

#DM814x-HT5
PREFERRED_VERSION_linux-omap3-ht5 = "2.6.37-04.04.00.01"
PREFERRED_VERSION_u-boot-omap3-ht5 = "04.04.00.01"
PREFFERED_VERSION_omap3-sgx-modules = "1.6.16.4117"

#DM365-HTC
PREFERRED_VERSION_linux_davinci_htc = "2.6.32.17-03.01.01.39"
PREFERRED_VERSION_u-boot-davinci-htc = "03.21.00.03"
PREFERRED_VERSION_kernel = "2.6.32.17-03.01.01.39"
PREFERRED_VERSION_watchdog="1.2.3"

# hanover-products/${PRODUCT}/${RELDIR}/libs.inc
pf=${LIB_BUILD_MODE}

TASK_HANOVER_GLIBC  =          "\
    glibc                       \
    glibc-utils                 \
    libsegfault                 \
    glibc-thread-db             \
    libgcc                      \
    libstdc++                   \
"

TASK_HANOVER_LIBS   =         "\
    alsa-lib${pf}              \
    jpeg${pf}                  \ 
    lzo${pf}                   \
    libopkg${pf}               \
    libpng${pf}                \
    readline${pf}              \
    libusb-compat${pf}         \
    libusb1${pf}               \
    cdparanoia${pf}            \
    zlib${pf}                  \
    ncurses${pf}               \
    expat${pf}                 \
    libcdaudio${pf}            \
    libcddb${pf}               \
    libcdio${pf}               \
    libffi${pf}                \
    libid3tag${pf}             \
    libmad${pf}                \
    liboil${pf}                \
    taglib${pf}                \
    zlib${pf}                  \
"

TASK_HANOVER_BOOST_LIBS   =     "\
   boost${pf}                    \
"

TASK_HANOVER_SDL_LIBS =        "\
    libsdl-gfx${pf}           \
    libsdl-image${pf}         \
    libsdl-mixer${pf}         \
    libsdl-ttf${pf}           \
    libsdl-net${pf}           \
  "

TASK_HANOVER_NCURSES_LIBS =     "\
    ncurses${pf}                 \
  "

TASK_HANOVER_APPS_LIBS =        "\
  jsoncpp${pf}                   \
"

export TASK_HANOVER_PRODUCT_LIBS =  "\
    ${TASK_HANOVER_GLIBC}           \
    ${TASK_HANOVER_LIBS}            \
    ${TASK_HANOVER_BOOST_LIBS}      \
    ${TASK_HANOVER_SDL_LIBS}        \
    ${TASK_HANOVER_NCURSES_LIBS}    \
    ${TASK_HANOVER_APPS_LIBS}       \
"

###############################################################################
#   Preferred versions must be define in this section                         #
###############################################################################
#PREFERRED_VERSION_jsoncpp${pf} = "0.5.0"
#PREFERRED_VERSION_sqlitewrapped = "1.3.1"
#PREFERRED_VERSION_libgles-omap3 = "4.04.00.02"
#PREFERRED_VERSION_net-snmp-agentx++ = "1.0.7"

# Runtime dependencies 
RDEPENDS =                            "\
      ${TASK_HANOVER_COMMON_RDEPENDS}        \
      ${TASK_HANOVER_PRODUCT}                \
      ${TASK_HANOVER_PRODUCT_LIBS}           \
      "
DEPENDS = ""
