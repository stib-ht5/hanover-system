DESCRIPTION = "Hanover Displays Foundation Libraries for Transport Computers"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "Proprietary"

inherit autotools pkgconfig

RRECOMMENDS += "virtual/libhtc-board"

INC_PR = "r2"
PR = "${INC_PR}"

# m4/ax_*.m4
EXTRA_OECONF_append = " --without-autoconf-archive"

# we need to split the tools into a different package
# in order to get a libhtc-core0 package when there are
# tools in /bin
#
PACKAGES =+ "\
	htcdev-dbg \
	htcdev \
	libhtc-utils-dbg \
	libhtc-utils \
"

FILES_htcdev = "${bindir}/htcdev"
FILES_htcdev-dbg = "${bindir}/.debug/htcdev"

FILES_libhtc-utils = "${bindir}"
FILES_libhtc-utils-dbg = "${bindir}/.debug"

PROVIDES += "\
	htcdev \
	libhtc-utils \
"

RDEPENDS_htcdev-dbg += "htcdev"
RDEPENDS_libhtc-utils += "htcdev"
RDEPENDS_libhtc-utils-dbg += "libhtc-utils htcdev-dbg"
