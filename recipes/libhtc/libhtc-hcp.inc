inherit autotools pkgconfig

DEPENDS += "libhtc-core"

INC_PR = "r1"
PR = "${INC_PR}"

PACKAGES =+ "\
	${PN}-tests-dbg \
	${PN}-tests \
	"

FILES_${PN}-tests-dbg += "${libexecdir}/*/.debug"
FILES_${PN}-tests += "${libexecdir}"
