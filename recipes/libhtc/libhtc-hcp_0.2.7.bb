require ${PN}.inc

EXTRA_OECONF += "--enable-release"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/libhtc/${BPN}/${BPV}/${BPN}-${PV}.tar.xz;name=v${PV}"

LIC_FILES_CHKSUM = "file://LICENCE;md5=e9057d72d61435cdffd17e266ddc45d9"

SRC_URI[v0.2.7.md5sum] = "1b4ab3e2130b940e6522507bf5a129ec"
SRC_URI[v0.2.7.sha256sum] = "1446ade7ca3385dc2ea8f73fe8232604db5a184ed84035b6423495d51e715ab9"
