inherit autotools pkgconfig

DEPENDS += "libhtc-core"
DEPENDS += "alsa-lib"
DEPENDS += "ffmpeg"

INC_PR = "r2"
PR = "${INC_PR}"

FILES_${PN}-dbg += "${libexecdir}/*/.debug"
