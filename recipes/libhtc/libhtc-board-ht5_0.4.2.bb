require ${PN}.inc

BRANCH = "master"
SRCREV = "${PV}"

PR .= ".1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/libhtc/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v0.4.2.md5sum] = "ed55ea07f6e841bb0272a9129ed94f03"
SRC_URI[v0.4.2.sha256sum] = "e8733792322249c4e5e48ef2edec5e5b7db93d6e22048681204b614a35a61b80"

S = "${WORKDIR}/git"
