DESCRIPTION = "script bindings for libhtc"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "Proprietary"

DEPENDS += "libhtc-core python lua5.1"
PROVIDES += "libhtc-python libhtc-lua"

inherit autotools 

INC_PR = "r1"
PR = "${INC_PR}"

