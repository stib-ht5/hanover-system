DESCRIPTION = "HDL HT2/HT3 board support for libhtc"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "Proprietary"

COMPATIBLE_MACHINE = "dm365-htc"
PACKAGE_ARCH = "${MACHINE_ARCH}"

DEPENDS   += "libhtc-core libhtc-audio"
PROVIDES  += "virtual/libhtc-board"
RPROVIDES_${PN} += "virtual/libhtc-board"

inherit autotools pkgconfig

INC_PR = "r2"
PR = "${INC_PR}"

FILES_${PN}-dbg += "${libdir}/htc/.debug"
FILES_${PN}-dev += "${libdir}/htc/*.la"
FILES_${PN} += "${libdir}/htc/*.so"
