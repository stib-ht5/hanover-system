DESCRIPTION = "HTC API compatibility"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "Proprietary"

DEPENDS += "libhtc-core"
PROVIDES += "virtual/htc-api"
RREPLACES_${PN} = "htc-api"

inherit autotools pkgconfig

INC_PR = "r1"
PR = "${INC_PR}"

# split tests
PACKAGES =+ "${PN}-tests-dbg ${PN}-tests"
FILES_${PN}-tests = "${libexecdir}/${PN}"
FILES_${PN}-tests-dbg = "${libexecdir}/${PN}/.debug"
