require ${PN}.inc

EXTRA_OECONF += "--enable-release"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/libhtc/${BPN}/${BPV}/${BPN}-${PV}.tar.xz;name=v${PV}"

LIC_FILES_CHKSUM = "file://LICENCE;md5=468816fdd13cdb2720ece68c1017212e"

SRC_URI[v0.0.17.md5sum] = "b0d67829018e9bf7f0c29d5947c967e8"
SRC_URI[v0.0.17.sha256sum] = "40ddcdb7cd7a3bec4933a04e8b41115315eeac745b210ed00fe3a06386672679"
