require ${PN}.inc

BRANCH = "master"
SRCREV = "${PV}"

SRC_URI = "git://${HANOVER_GIT}/libhtc/${PN}.git;protocol=http;branch=${BRANCH}"
S = "${WORKDIR}/git"
