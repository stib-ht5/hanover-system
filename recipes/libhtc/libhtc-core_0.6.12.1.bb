require ${PN}.inc

EXTRA_OECONF += "--enable-release"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/libhtc/${BPN}/${BPV}/${BPN}-${PV}.tar.xz;name=v${PV}"

LIC_FILES_CHKSUM = "file://LICENCE;md5=468816fdd13cdb2720ece68c1017212e"

SRC_URI[v0.6.12.1.md5sum] = "de97ec348ffa0822fd0e0f5211be360b"
SRC_URI[v0.6.12.1.sha256sum] = "fdcd51bb5edde276e7be4d2a6bd856a05c7dfe1c838cef6aa2196c2fb4b02ae7"
