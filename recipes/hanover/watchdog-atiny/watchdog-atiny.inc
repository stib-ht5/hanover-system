DESCRIPTION = "HTC A-Tiny Watchdog"
HOMEPAGE = "http://www.hanoverdisplays.com"
LICENSE = "GPL2"
SECTION = "system"
DEPENDS = ""

COMPATIBLE_MACHINE = "dm365-htc"

EXTRA_OEMAKE = "CROSSCOMPILE=${TARGET_PREFIX}"
EXTRA_OEMAKE += "CC=${TARGET_PREFIX}gcc"

BRANCH ?= "master"
SRC_URI = "git://${HANOVER_GIT_LEGACY}/system-apps/watchdog.git;protocol=git;branch=${BRANCH}"

S = "${WORKDIR}/git"

do_compile() {
  oe_runmake all
 }

do_install() {
  install -d ${D}${sysconfdir}/init.d
  install -d ${D}${sysconfdir}/rc5.d
  install -d ${D}/sbin
  install -d ${D}/bin
  install -d ${D}/boot
  
  install -m 0755 ${S}/daemon/wdog_daemon ${D}/sbin
  install -m 0755 ${S}/utils/loadwdog     ${D}/sbin
  install -m 0755 ${S}/utils/safemode     ${D}/bin
  install -m 0755 ${S}/utils/unsafemode   ${D}/bin
  install -m 0755 ${S}/utils/wdogkick     ${D}/bin
  install -m 0755 ${S}/utils/wdog_ignignore_off  ${D}/bin
  install -m 0755 ${S}/utils/wdog_ignignore_on   ${D}/bin
  install -m 0755 ${S}/init.d/watchdog    ${D}${sysconfdir}/init.d
  install -m 0755 ${S}/init.d/wdogack     ${D}${sysconfdir}/init.d
  install -m 0755 ${S}/init.d/wdogreboot  ${D}${sysconfdir}/init.d
  install -m 0755 ${S}/init.d/wdogstandby ${D}${sysconfdir}/init.d
  
  ln -sf ../init.d/watchdog ${D}${sysconfdir}/rc5.d/S15watchdog 
  
  install -m 0755 ${S}/firmware/htc_wdog.bin ${D}/boot
  install -m 0755 ${S}/bootloader/wdog_boot.bin ${D}/boot
  install -m 0755 ${S}/bootloader_app/wdog_boot_app.bin ${D}/boot
}

PACKAGES = "${PN}"

#FILES_${PN} = "/sbin/wdog_daemon"
#FILES_${PN} += "/sbin/loadwdog"
#FILES_${PN} += "/bin/safemode"
#FILES_${PN} += "/bin/wdogkick"
#FILES_${PN} += "/etc/init.d/*"
#FILES_${PN} += "/etc/rc5.d/S15watchdog"
#FILES_${PN} += "/boot/htc_wdog.bin"

FILES_${PN} = "/sbin ${sysconfdir} /bin /boot"


INSANE_SKIP_${PN} = True 