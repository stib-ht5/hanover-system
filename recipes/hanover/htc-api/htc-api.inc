# vim: ft=sh ts=2 sw=2 et:
DESCRIPTION = "HTC API"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "GPLv2"

PR="r2"

COMPATIBLE_MACHINE = "dm365-htc"
PACKAGE_ARCH = "${MACHINE_ARCH}"

PROVIDES = "virtual/htc-api"
DEPENDS = "linux-davinci-htc"

export KERNEL_SRC = "${STAGING_DIR}/dm365-htc-arago-linux-gnueabi/kernel"

BRANCH ?= "master"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/system-apps/htc-api.git;protocol=git;branch=${BRANCH}"

S = "${WORKDIR}/git"

inherit autotools 

EXTRA_OECONF = "--enable-tests"

do_configure_append () {
  autoreconf -i
}

do_install_append () {
  install -d ${D}${bindir}/tests
 
  for x in ${S}/tests/test_*; do
    case "$x" in
    *.c|*.o) ;;
    *)
      install -m 0775 "$x" ${D}${bindir}/tests/
      ;;
    esac
  done
}

PACKAGES  = "${PN}-tests"
PACKAGES += "${PN}-dev ${PN}-static ${PN}-dbg ${PN}"

FILES_${PN}-tests = "${bindir}/tests/*"
