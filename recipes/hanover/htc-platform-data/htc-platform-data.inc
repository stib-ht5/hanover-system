DESCRIPTION = "HTC platform configuration data"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "GPLv2"

DEPENDS = ""

COMPATIBLE_MACHINE = "dm365-htc"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/system-apps/${PN}.git;protocol=git;branch=${BRANCH}"

S = "${WORKDIR}/git"

do_compile () {
 :
}

do_install () {
 install -d ${D}${sysconfdir}
 install -d ${D}/var/lib/alsa
 
 #install -m 0775 ${S}/fb.modes          ${D}${sysconfdir}
 install -m 0775 ${S}/asound.state      ${D}/var/lib/alsa
}

PACKAGES = "${PN}"

#FILES_${PN} = "${sysconfdir}/fb.modes"
FILES_${PN} += "/var/lib/alsa/asound.state"

#Skip QA
INSANE_SKIP_${PN} = True

