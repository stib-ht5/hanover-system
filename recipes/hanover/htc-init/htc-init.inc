DESCRIPTION = "HTC init scripts"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "GPLv2"

DEPENDS = ""

COMPATIBLE_MACHINE = "dm365-htc"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/system-apps/${PN}.git;protocol=git;branch=${BRANCH}"

S = "${WORKDIR}/git"

do_compile () {
 :
}

do_install () {
 install -d ${D}${sysconfdir}/init.d
 install -d ${D}${sysconfdir}/rcS.d
 install -d ${D}${sysconfdir}/rc0.d
 install -d ${D}${sysconfdir}/rc3.d
 install -d ${D}${sysconfdir}/rc6.d
 
 install -m 0775 ${S}/ampoff            ${D}${sysconfdir}/init.d
 install -m 0775 ${S}/blankoff          ${D}${sysconfdir}/init.d
 install -m 0775 ${S}/htcalsa           ${D}${sysconfdir}/init.d
 
 ln -sf ../init.d/ampoff                ${D}${sysconfdir}/rc0.d/S15ampoff
 ln -sf ../init.d/ampoff                ${D}${sysconfdir}/rc6.d/S15ampoff
 ln -sf ../init.d/blankoff              ${D}${sysconfdir}/rcS.d/S60blankoff
 ln -sf ../init.d/htcalsa               ${D}${sysconfdir}/rc3.d/S50htcalsa
}

PACKAGES = "${PN}"

FILES_${PN} = "${sysconfdir}/init.d/ampoff"
FILES_${PN} += "${sysconfdir}/init.d/blankoff"
FILES_${PN} += "${sysconfdir}/init.d/htcalsa"
FILES_${PN} += "${sysconfdir}/rc0.d/S15ampoff"
FILES_${PN} += "${sysconfdir}/rc6.d/S15ampoff"
FILES_${PN} += "${sysconfdir}/rcS.d/S60blankoff"
FILES_${PN} += "${sysconfdir}/rc3.d/S50htcalsa"

#Skip QA
INSANE_SKIP_${PN} = True
