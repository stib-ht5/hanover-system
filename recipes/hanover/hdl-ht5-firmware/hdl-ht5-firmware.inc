DESCRIPTION = "HT5 EC Firmware"
HOMEPAGE = "http://www.hanoverdisplays.com"
LICENSE  = "Proprietary"
SECTION  = "system"

COMPATIBLE_MACHINE = "dm814x-ht5"
PACKAGE_ARCH = "${MACHINE_ARCH}"

BOARD_REVS = "00"
SRC_URI = "${HANOVER_RELEASES}/SW-HT5-FIRMWARE/v${PV}/hdl-ht5-00-v${PV}.bin"

do_configure() {
	:
}

do_compile() {
	:
}

do_install() {
	install -d "${D}/lib/firmware"
	for x in ${BOARD_REVS}; do
		install -m 0644 "${WORKDIR}/hdl-ht5-$x-v${PV}.bin" "${D}/lib/firmware/"
		ln -sf "hdl-ht5-$x-v${PV}.bin" "${D}/lib/firmware/hdl-ht5-$x.bin"
	done
}

FILES_${PN} = "/lib/firmware"
