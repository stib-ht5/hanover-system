DESCRIPTION = "HTC utils: beep, devmem3, haepr, setamp, setpot, setswitch"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "GPLv2"

DEPENDS = ""

COMPATIBLE_MACHINE = "dm365-htc"
EXTRA_OEMAKE = "CROSSCOMPILE=${TARGET_PREFIX}"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/system-apps/${PN}.git;protocol=git;branch=${BRANCH}"


do_install_append () {
 install -d ${D}${bindir}

 install -m 775 ${S}/beep               ${D}${bindir}
 install -m 775 ${S}/devmem3            ${D}${bindir}
 install -m 775 ${S}/hanepr             ${D}${bindir}
 install -m 775 ${S}/setamp             ${D}${bindir}
 install -m 775 ${S}/setpot             ${D}${bindir}
 install -m 775 ${S}/setswitch          ${D}${bindir}
}

FILES_${PN} = "${bindir}/beep"
FILES_${PN} += "${bindir}/devmem3"
FILES_${PN} += "${bindir}/hanepr"
FILES_${PN} += "${bindir}/setamp"
FILES_${PN} += "${bindir}/setpot"
FILES_${PN} += "${bindir}/setswitch"

#Skip QA
INSANE_SKIP_${PN} = True
