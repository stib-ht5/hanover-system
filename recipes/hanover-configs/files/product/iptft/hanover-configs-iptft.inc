#Hanover configuration files package
do_install_prepend() {
    
    install -d ${D}${sysconfdir}
    install -d ${D}/home/root
    install -d ${D}/media
    install -d ${D}/usr/share
    
    # autofs
    install -m 0644 ${S}/product/${PRODUCT}/auto.master ${D}/${sysconfdir}/
    install -m 0644 ${S}/product/${PRODUCT}/auto.nfs ${D}/${sysconfdir}/
   
    # Creating data autofs mount point
    ln -snf "/media/nfs/data" "${D}/usr/share/data"
    
    # Creating update autofs mount point    
    ln -snf "nfs/update/obc-iv/ipk/ip_tft" "${D}/media/update"
    
    if [ ${MACHINE} = "dm814x-z3" ] ; then
        install -m 0755 ${S}/machine/${MACHINE}/tft_resolution.conf   ${D}${sysconfdir}/
        install -m 0755 ${S}/machine/${MACHINE}/gstplay.sh   ${D}/home/root/
        install -m 0755 ${S}/machine/${MACHINE}/rtsplay.sh   ${D}/home/root/
    fi
}

PACKAGES += "${PN}-iptft"

FILES_${PN}-iptft  = "${sysconfdir}/auto.master"
FILES_${PN}-iptft  += "${sysconfdir}/auto.nfs"
FILES_${PN}-iptft  += "/usr/share/data /media/nfs /media/update" 
INSANE_SKIP_${PN}-iptft = True

CONFFILES_${PN}-iptft =             "\
  ${sysconfdir}/auto.master          \
  ${sysconfdir}/auto.nfs             \
"

PACKAGES += "${PN}-${MACHINE}"

FILES_${PN}-dm814x-z3 =  "/home/root/gstplay.sh"
FILES_${PN}-dm814x-z3 += "/home/root/rtsplay.sh" 
FILES_${PN}-dm814x-z3 += "${sysconfdir}/tft_resolution.conf" 
INSANE_SKIP_${PN}-dm814x-z3 = True

CONFFILES_${PN}-dm814x-z3 =             "\
  ${sysconfdir}/tft_resolution.conf      \
"
