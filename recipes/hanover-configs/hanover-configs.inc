#Hanover configuration files package
# vim: set ft=sh ts=2 sw=2 et:

LICENSE = "MIT"

PR = "r3"
PE = "0"

DEPENDS = "quilt"

PACKAGES="${PN}"
PACKAGE_ARCH = "${MACHINE_ARCH}"

BRANCH = "master"
SRCREV = "${PV}"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/oe/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}" 

SRC_URI[v1.2.3.md5sum] = "738a1ee11fa9f6d874811ce0dbd2803f"
SRC_URI[v1.2.3.sha256sum] = "00c9ad6ac693c50f296c02fb214dedc33064f655a1fd62340686e33acecb4aa2"

S = "${WORKDIR}/git"


do_configure () {
  :
}
 
do_compile () {
  :
}

do_install () {
  oe_runmake install BOARD=${MACHINE} DESTDIR=${D}
}



do_deploy () {
  :
}

do_deploy[dirs] = "${S}"
addtask deploy before do_package_stage after do_compile

FILES_${PN} = "${bindir}"
FILES_${PN} += "${sysconfdir}"
FILES_${PN} += "/var/service"
INSANE_SKIP_${PN} = True

require ${FILE_DIRNAME}/files/product/${PRODUCT}/hanover-configs-${PRODUCT}.inc
