# vim: set ft=sh:

LICENSE = "Proprietary"

PACKAGES = "${PN}"
PACKAGE_ARCH = "${MACHINE_ARCH}"
INC_PR = "r4"

INHIBIT_DEFAULT_DEPS = "1"

do_configure() {
	:
}

do_compile() {
	local x= dev=

	if [ "${RELDIR}" = "dev" ]; then
		dev=true
	fi

	x="$(date -u "+%Y-%m-%d %H:%M %Z")"
	echo "Build Date: $x" > hanover.version

	if [ -n "${HANOVER_VERSION_PRODUCT}" ]; then
		# environment for release scripts
		cat <<EOT > buildenv.inc
export RELDIR=${RELDIR}
export BUILD_PURPOSE=${BUILD_PURPOSE}
export PRODUCT=${PRODUCT}
export PRODUCT_RELEASE=${PRODUCT_RELEASE}
export MACHINE=${MACHINE}
export FEED_ARCH=${FEED_ARCH}
export OEBASE="\$(cd ../../../../../../../../ && pwd -P)"
EOT

		cat <<EOT >> hanover.version
Product ID: ${PRODUCT}
Product release: ${PRODUCT_RELEASE}
EOT

		echo "Build Date: $x" > build.date
	fi

	cat <<EOT >> hanover.version
Hanover Distro: ${HANOVER_VERSION}
Machine: ${MACHINE}
CPU instruction set: ${FEED_ARCH}
Compiler: ${TARGET_SYS}
Build purpose: ${BUILD_PURPOSE}
Build type: ${RELDIR}

Versions:
EOT
	for x in arago arago-bitbake arago-oe-dev \
		hanover-apps ${dev:+hanover-apps-dev} \
		hanover-system ${dev:+hanover-system-dev} \
		${HANOVER_VERSION_PRODUCT:+hanover-products/${PRODUCT}} \
		; do

		echo "$x: $(cd "${OEBASE}/$x" && git describe | sed -e "s|^${x}_||")"
	done >> hanover.version
}

do_install() {
	install -d "${D}${sysconfdir}"
	install -m 0644 hanover.version "${D}${sysconfdir}/${PN}"

	if [ -n "${HANOVER_VERSION_PRODUCT}" ]; then
		ln -sf "${PN}" "${D}${sysconfdir}/hanover.version"
		install -m 0644 build.date "${D}${sysconfdir}/"
	fi
}

do_deploy () {
	install -d "${DEPLOY_DIR_IMAGE}"
	install -m 0644 hanover.version "${DEPLOY_DIR_IMAGE}/${PN}"
	package_stagefile_shell "${DEPLOY_DIR_IMAGE}/${PN}"

	if [ -n "${HANOVER_VERSION_PRODUCT}" ]; then
		install -d "${DEPLOY_DIR_IMAGE}/scripts"
		install -m 0644 buildenv.inc "${DEPLOY_DIR_IMAGE}/scripts/"
		ln -snf buildenv.inc "${DEPLOY_DIR_IMAGE}/scripts/buildenv.sh"
		package_stagefile_shell "${DEPLOY_DIR_IMAGE}/scripts"/buildenv.*
	fi
}


do_deploy[dirs] = "${S}"
addtask deploy before do_package_stage after do_install

FILES_${PN}  = "${sysconfdir}"
