require hanover-version.inc

PN = "hanover-${PRODUCT}-version"
PV = "${PRODUCT_RELEASE}"
PR = "${INC_PR}.0"

HANOVER_VERSION_PRODUCT = "1"

RDEPENDS += "hanover-version"
