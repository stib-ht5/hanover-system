require ${OEBASE}/arago/recipes/ti/ti-firmware-dm814x.inc


COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"


SRCREV = "firmware-dm814x_${PV}"
BRANCH = "master"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v05.02.00.46.md5sum] = "8592b641d41a47a8fe9ee373c9d9f8e0"
SRC_URI[v05.02.00.46.sha256sum] = "e7fb0e5e3984e0e42f035f0a404f7cce202d4e9bd9967a0419935725e7c59a62"

S = "${WORKDIR}/git"
