DESCRIPTION = "TI AAC LC Decoder for C674x devices"
SECTION = "multimedia"
LICENSE = "TI Proprietary"

require ${OEBASE}/arago/recipes/ti/ti-paths.inc
require ${OEBASE}/arago/recipes/ti/ti-staging.inc

PVExtra="_elf"

SRCREV = "${PN}_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v01.41.00.00.md5sum] = "84525b8e2ffd1b83b1c2a6fc3f8febb7"
SRC_URI[v01.41.00.00.sha256sum] = "75e82eb55d7054ced220c84a8c590b52a93908f87d616d77a06b683c57515fa3"

do_install() {
    install -d ${D}${CODEC_INSTALL_DIR_RECIPE}
    cp -pPrf ${S}/* ${D}${CODEC_INSTALL_DIR_RECIPE}
}
