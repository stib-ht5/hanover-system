SRCREV = "codec_engine_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v3.22.01.06.md5sum] = "843fff02d63cdd65c2eca8e4f3ac7cbe"
SRC_URI[v3.22.01.06.sha256sum] = "e5bb23c57401c4a9f7a1f3d03fe0b15658178019eafa618da8d28af053f5bbcc"
