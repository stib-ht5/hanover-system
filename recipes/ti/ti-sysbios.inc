DESCRIPTION = "TI SYS/BIOS v6 Kernel"
HOMEPAGE = "http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/bios/sysbios"
SECTION = "devel"
LICENSE = "BSD"

require ${OEBASE}/arago/recipes/ti/ti-paths.inc
require ${OEBASE}/arago/recipes/ti/ti-staging.inc

PR = "r6"
PVExtra = ""

SRCREV = "bios_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v6.33.05.46.md5sum] = "e01e8bcf8ef7f25b3461bef6c2df82e2"
SRC_URI[v6.33.05.46.sha256sum] = "ab9af078f268bff5b8cece44c09a1fe525952e0904df3aaea8340d0eb0f487f0"

do_install() {
    install -d ${D}${SYSBIOS_INSTALL_DIR_RECIPE}
    cp -pPrf ${S}/* ${D}${SYSBIOS_INSTALL_DIR_RECIPE}
}

