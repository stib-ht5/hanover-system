require ${OEBASE}/arago/recipes/ti/ti-media-controller-utils.inc

COMPATIBLE_MACHINE = "(dm814x-.*)"

SRCREV = "ti-media-controller_${PV}"
BRANCH = "master"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV} \
          file://patches \
          "
SRC_URI[v3.00.00.05.md5sum] = "870d2eed2e0fbbe0bed0b677e7353f8e"
SRC_URI[v3.00.00.05.sha256sum] = "70ee862adc7636d0e59e94d8b480a033ed16f3b62ee5c84b54918b4059406a8e"

S = "${WORKDIR}/git"

#Due to some reasons do_patch is not invoked for this recipe...
do_patch() {
   quilt push 01-media-controller-cc-fix.patch
}

FILES_${PN} += "${installdir}/ti-mcutils-tree/src/mm_host_util/mm_dm81xxbm_512M.bin" 
FILES_${PN} += "${installdir}/ti-mcutils-tree/src/mm_host_util/mm_dm81xxbm.bin"
