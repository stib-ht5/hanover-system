DESCRIPTION = "TI OMX Components"
SECTION = "devel"
LICENSE = "BSD"

require ${OEBASE}/arago/recipes/ti/ti-omx.inc


COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"

PR = "r1"

SRCREV = "omx_${PV}"
BRANCH = "master"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV} \
	   file://sdk.mk \
           file://patches \
"
SRC_URI[v05.02.00.48.md5sum] = "06ef396584272576a3f3d6e3f61c6516"
SRC_URI[v05.02.00.48.sha256sum] = "e930bc2cc014a8bc8d1b61e5765d8dd36d7a774cc022dd19600b1108f205ab19"

S = "${WORKDIR}/git"

#Due to some reasons do_patch is not invoked for this recipe...
do_patch() {
   quilt push 01-omx-cc-fix.patch
}



do_prepsources() {
    cp ${WORKDIR}/sdk.mk ${S}
}

addtask prepsources after do_unpack before do_patch
