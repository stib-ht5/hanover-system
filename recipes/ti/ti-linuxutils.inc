require ${OEBASE}/arago/recipes/ti/ti-linuxutils.inc

PE = "1"

PR = "r1"

SRCREV = "linuxutils_${PV}"
BRANCH = "master"


S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"
SRC_URI += "file://sdk.mk \
"

SRC_URI[v3.22.00.02.md5sum] = "8120c20852bee949fc46f0870de0f171"
SRC_URI[v3.22.00.02.sha256sum] = "fc9de98f045eebb02c45a0cb06b77e1c87d097ccca4fcc23f3217b068602c56f"

do_prepsources() {
    cp ${WORKDIR}/sdk.mk ${S}
}
addtask prepsources after do_unpack before do_patch
