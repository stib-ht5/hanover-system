require ${OEBASE}/arago/recipes/ti/ti-syslink.inc

COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"

PR = "r1"

SRCREV = "syslink_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v2.20.02.20.md5sum] = "9f6aff6b6754cc0f34d5ac422ab2ad92"
SRC_URI[v2.20.02.20.sha256sum] = "5850cf56afd603fbf0e552ebc57908494dc7a62ab007ce6e9406473f5006419a"

do_install_append_dm814x-z3 () {
    install -d ${D}/${installdir}/syslink-examples/TI814X/helloworld
    install ${D}/usr/share/ti/examples/ex01_helloworld/release/* ${D}/${installdir}/syslink-examples/TI814X/helloworld
    install -d ${D}/${installdir}/syslink-examples/TI814X/messageq
    install ${D}/usr/share/ti/examples/ex02_messageq/release/* ${D}/${installdir}/syslink-examples/TI814X/messageq
    install -d ${D}/${installdir}/syslink-examples/TI814X/notify
    install ${D}/usr/share/ti/examples/ex03_notify/release/* ${D}/${installdir}/syslink-examples/TI814X/notify
    install -d ${D}/${installdir}/syslink-examples/TI814X/sharedregion
    install ${D}/usr/share/ti/examples/ex04_sharedregion/release/* ${D}/${installdir}/syslink-examples/TI814X/sharedregion
    install -d ${D}/${installdir}/syslink-examples/TI814X/heapbufmp 
    install ${D}/usr/share/ti/examples/ex05_heapbufmp/release/* ${D}/${installdir}/syslink-examples/TI814X/heapbufmp
    install -d ${D}/${installdir}/syslink-examples/TI814X/listmp
    install ${D}/usr/share/ti/examples/ex06_listmp/release/* ${D}/${installdir}/syslink-examples/TI814X/listmp
    install -d ${D}/${installdir}/syslink-examples/TI814X/gatemp
    install ${D}/usr/share/ti/examples/ex07_gatemp/release/* ${D}/${installdir}/syslink-examples/TI814X/gatemp
    install -d ${D}/${installdir}/syslink-examples/TI814X/ringio
    install ${D}/usr/share/ti/examples/ex08_ringio/release/* ${D}/${installdir}/syslink-examples/TI814X/ringio
  }

  
do_install_append_dm814x-stib () {
    install -d ${D}/${installdir}/syslink-examples/TI814X/helloworld
    install ${D}/usr/share/ti/examples/ex01_helloworld/release/* ${D}/${installdir}/syslink-examples/TI814X/helloworld
    install -d ${D}/${installdir}/syslink-examples/TI814X/messageq
    install ${D}/usr/share/ti/examples/ex02_messageq/release/* ${D}/${installdir}/syslink-examples/TI814X/messageq
    install -d ${D}/${installdir}/syslink-examples/TI814X/notify
    install ${D}/usr/share/ti/examples/ex03_notify/release/* ${D}/${installdir}/syslink-examples/TI814X/notify
    install -d ${D}/${installdir}/syslink-examples/TI814X/sharedregion
    install ${D}/usr/share/ti/examples/ex04_sharedregion/release/* ${D}/${installdir}/syslink-examples/TI814X/sharedregion
    install -d ${D}/${installdir}/syslink-examples/TI814X/heapbufmp 
    install ${D}/usr/share/ti/examples/ex05_heapbufmp/release/* ${D}/${installdir}/syslink-examples/TI814X/heapbufmp
    install -d ${D}/${installdir}/syslink-examples/TI814X/listmp
    install ${D}/usr/share/ti/examples/ex06_listmp/release/* ${D}/${installdir}/syslink-examples/TI814X/listmp
    install -d ${D}/${installdir}/syslink-examples/TI814X/gatemp
    install ${D}/usr/share/ti/examples/ex07_gatemp/release/* ${D}/${installdir}/syslink-examples/TI814X/gatemp
    install -d ${D}/${installdir}/syslink-examples/TI814X/ringio
    install ${D}/usr/share/ti/examples/ex08_ringio/release/* ${D}/${installdir}/syslink-examples/TI814X/ringio
  }
