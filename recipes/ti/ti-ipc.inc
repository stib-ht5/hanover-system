DESCRIPTION = "TI Inter Process Ccommunication (IPC) Mechanisms (for Uni- and Multi- Processor Configurations)"
HOMEPAGE = "http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ipc/index.html"

SECTION = "devel"
LICENSE = "BSD" 

require ${OEBASE}/arago/recipes/ti/ti-paths.inc
require ${OEBASE}/arago/recipes/ti/ti-staging.inc


COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"

PR = "r5"

PVExtra = ""

SRCREV = "${PN}_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"
SRC_URI =+ "file://ipc.pc"

SRC_URI[v1.24.03.32.md5sum] = "cc815a4ee407fb18e99be313dc35457f"
SRC_URI[v1.24.03.32.sha256sum] = "0c631c53275e798c8feda7109c1c072e640e8702de5db3b50955ec2d39a1538c"

DEPENDS = "ti-sysbios ti-cgt6x ti-xdctools"

do_prepsources() {
    cd ${S}
    ln -sf packages/ti ti 
}
addtask prepsources after do_unpack before do_patch

do_configure() {
    sed -i -e s:@VERSION@:${PV}:g ${WORKDIR}/ipc.pc
    
    cp ${WORKDIR}/ipc.pc ${S}
}

do_install() {
    install -d ${D}${IPC_INSTALL_DIR_RECIPE}
    cp -pPrf ${S}/* ${D}${IPC_INSTALL_DIR_RECIPE}
   
    install -d ${D}/${libdir}/pkgconfig
    install ${S}/ipc.pc ${D}/${libdir}/pkgconfig/
}

FILES_ti-ipc-dev = "${libdir}/*"
