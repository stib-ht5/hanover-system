require ${OEBASE}/arago/recipes/ti/ti-uia.inc

PVExtra = ""

PR = "r1"

COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"

SRCREV = "uia_${PV}"
BRANCH = "master"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV} \
           file://sdk.mk \
           file://patches \
"
SRC_URI[v1.01.01.14.md5sum] = "166d6256b24b6410ee97ebc6b5d15a49"
SRC_URI[v1.01.01.14.sha256sum] = "9fef59b746f9c443630ba167a87729ce9feb8b0d6136e072519ae1d6dce1372c"

S = "${WORKDIR}/git"

RANLIB = "${TOOLCHAIN_LONGNAME}-ranlib"

do_prepsources() {
    cp ${WORKDIR}/sdk.mk ${S}
}
addtask prepsources after do_unpack before do_patch

do_patch() {
 quilt push 01-uia-makefile.patch
 quilt push 02-uia-lib-makefile.patch
}

INSANE_SKIP_${PN} = True
