require ${OEBASE}/arago/recipes/ti/ti-slog.inc

SRCREV = "slog_${PV}"
BRANCH = "master"

PR = "r1"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v04.00.00.02.md5sum] = "14405f92b5babb207812cc010f9f1741"
SRC_URI[v04.00.00.02.sha256sum] = "225a1f7d74ee192c9500b8af2e4ff3a693b4845bb6c4e44dd4971d6a5f05bb6a"
