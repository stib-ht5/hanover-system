require ${OEBASE}/arago/recipes/ti/gst-openmax-ti.inc



COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"

SRCREV = "${PN}_${PV}"
BRANCH = "master"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v07.00.00.md5sum] = "6010010401c78a3dbcfb7365f81e52cd"
SRC_URI[v07.00.00.sha256sum] = "b31051114d64b62c6c87448264d263e607ffadaf2f7bc59cd6e70808afce6862"

S = "${WORKDIR}/git"

EXTRA_AUTORECONF = ""
