require ${OEBASE}/arago/recipes/ti/ti-osal.inc

SRCREV = "osal_${PV}"
BRANCH = "master"

PR = "r1"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.22.01.09.md5sum] = "e1e7c5bd5d18c416cbfff8b2ebe275a1"
SRC_URI[v1.22.01.09.sha256sum] = "4249e9396326c0ca3717d04f582186b27de8267fa5eba07ebb8cd84935f67fe6"
