DESCRIPTION = "TI DSP Code Generation Tools"
HOMEPAGE = "https://www-a.ti.com/downloads/sds_support/TICodegenerationTools/download.htm"
SECTION = "devel"
LICENSE = "TI"

require ${OEBASE}/arago/recipes/ti/ti-paths.inc
require ${OEBASE}/arago/recipes/ti/ti-staging.inc

PR = "r5"

COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-stib|dm814x-ht5)"


SRCREV = "cgt6x_${PV}"
BRANCH = "master"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v7.3.4.md5sum] = "85f4eabacdc25582bfad96450f11f622"
SRC_URI[v7.3.4.sha256sum] = "77988443a2883b7ab6608a2d83189966c8792c2526478954140318d6e769baac"

S = "${WORKDIR}/git"


do_install() {
    install -d ${D}${CODEGEN_INSTALL_DIR_RECIPE}
    cp -pPrf ${S}/* ${D}${CODEGEN_INSTALL_DIR_RECIPE}
}

 
