require ${OEBASE}/arago/recipes/ti/ti-xdais.inc

SRCREV = "xdais_${PV}"
BRANCH = "master"

PR = "r1"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v7.22.00.03.md5sum] = "cf34fcf26b08e84c9d322aa6f7650ae4"
SRC_URI[v7.22.00.03.sha256sum] = "dc97b8691292e6df7e33f142096627474db85f3c85464d38da86c84086064321"
