DESCRIPTION = "TI Framework Components"
HOMEPAGE = "http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/fc"
SECTION = "devel"
LICENSE = "BSD"

require ${OEBASE}/arago/recipes/ti/ti-paths.inc
require ${OEBASE}/arago/recipes/ti/ti-staging.inc

COMPATIBLE_MACHINE = "(dm814x-z3|dm814x-evm|dm814x-ht5)"

PR = "r2"

PVExtra = ""

SRCREV = "framework-components_${PV}"
BRANCH = "master"

S = "${WORKDIR}/git"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/ti/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v3.22.01.07.md5sum] = "9f2410c7788f93d0376b455df9853c37"
SRC_URI[v3.22.01.07.sha256sum] = "972573a543cf8a8442629dad8abcd2809579a7041e983f1105dea3c48a373f83"

do_install() {
    install -d ${D}${FC_INSTALL_DIR_RECIPE}
    cp -pPrf ${S}/* ${D}${FC_INSTALL_DIR_RECIPE}
}

