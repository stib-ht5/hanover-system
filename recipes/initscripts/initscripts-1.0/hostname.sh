#!/bin/sh

set -u
hostname=

if [ -s /etc/hostname ]; then
	read hostname < /etc/hostname
fi

if [ -n "$hostname" ]; then
	hostname "$hostname"
else
	hostname "hdl-obc"
fi
