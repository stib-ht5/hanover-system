# Hanover security package includes Hanover R&D RSA key-pairs & customer specific
# keys and some product SSH configuration
DESCRIPTION = "Hanover security package"
HOMEPAGE = "http://www.hanoverdisplays.com"
SECTION = "system"
LICENSE = "MIT"

PR = "r2"
PE = "0"

PACKAGES = "${PN}"
PACKAGE_ARCH = "${MACHINE_ARCH}"

do_configure () {
  :
}
 
do_compile () {
  :
}

do_install () {
    install -d ${D}/home/root/.ssh
    
    touch ${D}/home/root/.ssh/authorized_keys 
    cat  ${S}/hanover.id_rsa.pub >>  ${D}/home/root/.ssh/authorized_keys
    cat  ${S}/customer.id_rsa.pub >>  ${D}/home/root/.ssh/authorized_keys
}
FILES_${PN} = "/home/root/.ssh"
INSANE_SKIP_${PN} = True

pkg_postinst_${PN} () {
    # Fix permissions
    chmod 0700 /home/root/.ssh
    chmod 0600 /home/root/.ssh/authorized_keys
}

include ./files/${PRODUCT}/hanover-security-${PRODUCT}.inc
