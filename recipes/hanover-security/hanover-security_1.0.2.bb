#Hanover configuration files package
require ${BPN}.inc

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/oe/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v1.0.2.md5sum] = "f633889bea03116904a7fa407b8b989d"
SRC_URI[v1.0.2.sha256sum] = "28a34a1ae3fe7824b434a3f7bb2f3b5f1572906eeb1e4d888c8508032addab9f"

BRANCH = "master"
SRCREV = "${PN}_${PV}"

S = "${WORKDIR}/git"
