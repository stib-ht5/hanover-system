HTC_DEPLOY_DIR ?= "${DEPLOY_DIR_IMAGE}/htc"

do_deploy () {

    install -d ${DEPLOY_DIR_IMAGE}
    install -d ${HTC_DEPLOY_DIR}
    
    install -m 755 ${S}/${PRODUCT}/hanover.ssh_config  ${HTC_DEPLOY_DIR}/ssh_config
    package_stagefile_shell            ${HTC_DEPLOY_DIR}/ssh_config
    
    install -m 755 ${S}/hanover.id_rsa  ${HTC_DEPLOY_DIR}/id_rsa
    package_stagefile_shell            ${HTC_DEPLOY_DIR}/id_rsa
}

do_deploy[dirs] = "${S}"
addtask deploy before do_package_stage after do_compile
