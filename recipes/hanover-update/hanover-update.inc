inherit base

DEPENDS = "python opkg"
LICENSE = "MIT"

PR = "r3"

PACKAGES="${PN}"
PACKAGE_ARCH = "all"

BRANCH = "master"
SRCREV = "${PV}"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/hanover-update/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v2.0.6.md5sum] = "db055c2299bf4abdd7d2579ea65a7b9c"
SRC_URI[v2.0.6.sha256sum] = "8a35af219ffe81e5d500dc563181d06df91f79a1b2d1f7b17a56c349443d1772"

S = "${WORKDIR}/git"

do_configure () {
  :
}
 
do_compile () {
  :
}

do_install () {
    install -D -m 0755 ${S}/src/opkg-upgrade-from.sh ${D}${bindir}/opkg-upgrade-from
}

do_deploy () {
    install -D -m 0755 ${S}/src/mkupdate.py ${DEPLOY_DIR_IMAGE}/scripts/
}

do_deploy[dirs]="${S}"
addtask deploy before do_package_stage after do_compile


FILES_${PN} += "${bindir}/opkg-upgrade-from"

INSANE_SKIP_${PN} = True
