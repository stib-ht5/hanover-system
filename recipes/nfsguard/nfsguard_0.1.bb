DESCRIPTION = "Simple runit service to watch nfsd"
LICENSE = "Proprietary"
PR = "r0"
PE = "0"

SRC_URI = "file://${PV}/run.sh"
S = "${WORKDIR}/${PV}"

do_compile() {
	:
}

do_install() {
	# install run script and reuse logrotate's logger
	#
	install -d ${D}${sysconfdir}/sv/${PN}/log
	install -m 0755 run.sh ${D}${sysconfdir}/sv/${PN}/run
	ln -snf ../../logrotate/log/run ${D}${sysconfdir}/sv/${PN}/log/run

	# enable runit service
	#
	install -d ${D}/var/service
	ln -snf /var/run/service/${PN} ${D}/var/service/${PN}
}
FILES = "${sysconfdir}/sv/${PN} /var/service/${PN}"
