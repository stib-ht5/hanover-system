#!/bin/sh

exec 2>&1
set -e

while true; do
	(
	echo "waiting for mountd's EPERM error"
	logread -f | stdbuf -o0 grep mountd | while read l; do
		echo "syslog: $l"
		case "$l" in
		*Operation\ not\ permitted*)
			echo "getfh problem caught. restarting mountd."
			killall rpc.mountd
			exit # because we are in a subshell
			;;
		esac
	done
	)

	nohup /usr/sbin/rpc.mountd -f /etc/exports &
done
