require recipes/tzdata/tzdata.inc

PR = "${INC_PR}.2"

SRC_URI[tar.md5sum] = "97d654f4d7253173b3eeb76a836dd65e"
SRC_URI[tar.sha256sum] = "6b288e5926841a4cb490909fe822d85c36ae75538ad69baf20da9628b63b692e"

# solar87 solar88 and solar89 are gone
#
TZONES = "\
	africa antarctica asia australasia europe northamerica southamerica \
	factory etcetera backward systemv \
"
# do not set a default timezone explicitly, only set to empty string
DEFAULT_TIMEZONE =
CONFFILES_${PN} =
