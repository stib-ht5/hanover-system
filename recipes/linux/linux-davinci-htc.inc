SECTION = "kernel"
DESCRIPTION = "Linux kernel Davinci DM365"
LICENSE = "GPLv2"

KERNEL_IMAGETYPE = "uImage"

CONFIG_NAME = "davinci_dm365htc_defconfig"

inherit kernel
require copy-defconfig.inc
require setup-defconfig.inc

BRANCH  ?= "branch.dev.HT2"
SRCREV  = "v${PV}"
SRC_URI = "git://${HANOVER_GIT}/ti-dm365/linux-davinci.git;protocol=git;branch=${BRANCH} \
           "

# Requires support for SOC_FAMILY matching in COMPATIBLE_MACHINE
COMPATIBLE_MACHINE = "dm365-htc"

S = "${WORKDIR}/git"

do_deploy_append() {
    install -d ${DEPLOY_DIR_IMAGE}
    
    #Creating symlinks
    cd ${DEPLOY_DIR_IMAGE}
    rm -f uImage.bin
    ln -sf ${KERNEL_IMAGE_BASE_NAME}.bin uImage.bin
    
    rm -f kmodules.tgz
    ln -sf ${MODULES_IMAGE_BASE_NAME}.tgz kmodules.tgz
}
