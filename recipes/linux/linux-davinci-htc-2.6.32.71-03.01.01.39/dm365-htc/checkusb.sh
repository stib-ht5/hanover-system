#!/bin/bash

# This is a template file and should not be run under andLinux,
# but only on the HTC.
#
# When found on the HTC (under /etc) it should have been processed through svnwcrev

# usage is Either:
# 1)
# checkusb (number)
#      number is how many seconds to sleep between checks
# or 2)
# checkusb --version
#      gives information about the version of this script


#####################################################################
# template parameters, to be filled in by developer before releasing
#####################################################################
APP_MAJOR=0
APP_MINOR=1
APP_RELEASE=4
APP_QUALIFIER="Alpha"

# Constants to filled in by svnwcrev before copying to image
############################################################
REPO_COMMIT_REV=1868
REPO_UPDATE_REV="1868"
REPO_MODIFIED=true
REPO_MIXED=false
REPO_DATE="2012/07/24 10:09:50"
REPO_DATE="2012/03/21 16:34:19"

# locations (relative to the pendrive mountpoint) of 'htcupdate' and allied files
###################################################################################
HTCUPDATE_REL_PATHNAME=hanover/update/htcupdate
UPDATE_SH_REL_PATHNAME=hanover/update/update.sh

# the pathname of the 'runonce' file the update script may use to pass back information
#######################################################################################
RUNONCE_PATHNAME=/tmp/runonce

usage() {
  echo `basename $0`": check for pendrive updates"
  echo "usage is either:"
  echo "   "`basename $0` "<waittimesecs>"
  echo "or"
  echo "   "`basename $0` "--version"
}

# a test to make sure that this script has been run through svnwcrev
if [ $REPO_MODIFIED != "true" ] && [ $REPO_MODIFIED != "false" ] ; then
  echo "Error evaluating svnwcrev substitutions - "
  echo " are you sure this script has been run through svnwcrev? EXITING."
  # exit 1
fi

# a crude test to see if we are running on the HTC:
# if [ ! -b /dev/mmcblk0 ] ; then
#   echo "You seem not to be running on an HTC - "
#   echo " this script is only designed for that environment. EXITING."
#   exit 1
# fi

# check for number of parameters
if [ $# -ne 1 ]; then
  echo "bad number of parameters"
  usage
  exit 1
fi

if [ $1 = '--version' ] ; then
  printf "Product Version: %d.%d.%d-%s\n" "$APP_MAJOR" "$APP_MINOR" "$APP_RELEASE" "$APP_QUALIFIER"
  printf "App Version: %d %s %s %s\n" "$REPO_COMMIT_REV" "$REPO_UPDATE_REV" "$REPO_MODIFIED" "$REPO_DATE"
  exit 0
fi

# parameter should be an integer; wait time in secs
if [[ "$1" =~ ^[0-9]+$ ]] ; then
  WAITTIMESECS=$1
else
  echo "Error: bad parameter '$1', integer expected" >&2
  exit 1
fi

# OK, now carry on with the main action
#######################################

# beep length (msecs)
SHORT_BEEP_TIMER=200
LONG_BEEP_TIMER=800

# main loop:
while (sleep $WAITTIMESECS); do
    # first see if we have a pendrive present
    # NB: only works if *one* pendrive present - could be /dev/sda(1) or /dev/sdb(1)
    PENDRIVE_DEVICE=/dev/sdb1
    grep `basename ${PENDRIVE_DEVICE}` /proc/partitions 2>/dev/null
    GREP_RV=$?
    if [ $GREP_RV -ne 0 ] ; then
        PENDRIVE_DEVICE=/dev/sda1
        grep `basename ${PENDRIVE_DEVICE}` /proc/partitions 2>/dev/null
        GREP_RV=$?
        if [ $GREP_RV -ne  0 ] ; then
            PENDRIVE_DEVICE=''
        fi
    fi

    if [ -n "${PENDRIVE_DEVICE}" ] ; then
        # unmount and attempt to remount the pendrive
	    umount ${PENDRIVE_DEVICE} 2>/dev/null
	    sleep 1

	    mount -t vfat ${PENDRIVE_DEVICE} /mnt/usb1 2>/dev/null 
        MOUNT_RV=$?
	    sleep 3

    	# if mount OK
    	if [ "$MOUNT_RV" -eq "0" ]; then
    		echo "mounted USB Key ${PENDRIVE_DEVICE}" | tee -a /var/log/checkusb

            # key detection announcement (1 beep)
            beep $SHORT_BEEP_TIMER

		    sleep 1
		    # check for existence of key file
		    if [ -e /mnt/usb1/${HTCUPDATE_REL_PATHNAME} ]; then                  
			    echo "Found HTC Update Key"
			    # check update script to execute
			    if [ -e /mnt/usb1/${UPDATE_SH_REL_PATHNAME} ]; then
				    echo "Executing update script"
				    # execute script
				    /mnt/usb1/${UPDATE_SH_REL_PATHNAME}
				    sync
			    else
				    # no update script
				    echo "No update script found on USB key"
			    fi

			    # unmount USB key
			    sleep 1
			    umount /mnt/usb1

			    # wait for key to be removed (ie. grep to fail)
			    GREP_RV=0
			    echo "Update done. Please remove USB Key"
			    while [ $GREP_RV -eq 0 ]; do
				    # beep
                                    beep $SHORT_BEEP_TIMER; beep $LONG_BEEP_TIMER
				    sleep 2
                                    grep `basename ${PENDRIVE_DEVICE}` /proc/partitions 2>/dev/null
                                    GREP_RV=$?
			    done

                            echo "USB Key removal detected"

                            # see if the script wants us to do something...
                            unset RUNONCE_AFTER_UPDATE
                            if [ -f ${RUNONCE_PATHNAME} ] ; then
                                echo "found ${RUNONCE_PATHNAME}, source'ing"
                                . /tmp/runonce   # may cause RUNONCE_AFTER_UPDATE to be set
                                echo "removing ${RUNONCE_PATHNAME}" 
                                rm ${RUNONCE_PATHNAME}
                            else
                                echo "${RUNONCE_PATHNAME} not found"
                            fi
                            unset RUNONCE
                            if [ -n "$RUNONCE_AFTER_UPDATE" ] ; then
                                echo "RUNONCE_AFTER_UPDATE =" $RUNONCE_AFTER_UPDATE
                                RUNONCE=$RUNONCE_AFTER_UPDATE
                                unset RUNONCE_AFTER_UPDATE
                                echo running $RUNONCE
                                $RUNONCE &
                            else
                                echo "RUNONCE_AFTER_UPDATE not set" 
                            fi

		    else
			    # no 'update' file found - not a valid key
			    echo "USB key is not a HTC Update Key"
			    umount /mnt/usb1
		    fi
        else
            : # echo "Failure mounting ${PENDRIVE_DEVICE}"
        fi
    else
        : # echo "no pendrive detected"
    fi

done




