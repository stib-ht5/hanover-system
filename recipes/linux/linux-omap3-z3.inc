SECTION = "kernel"
DESCRIPTION = "Linux kernel for OMAP3 EVM from PSP, based on linux-omap3 kernel"
LICENSE = "GPLv2"
KERNEL_IMAGETYPE = "uImage"

inherit kernel
require copy-defconfig.inc
require setup-defconfig.inc

# Used instead of PR revision element because it's how kernel class uses this convention:
MACHINE_KERNEL_PR_append = ".2"

BRANCH ?= "master"

SRC_URI = "git://${HANOVER_GIT_LEGACY}/bsp/${PN}.git;protocol=git;branch=${BRANCH} \
           file://defconfig \
           "

# Requires support for SOC_FAMILY matching in COMPATIBLE_MACHINE
COMPATIBLE_MACHINE = "dm814x-z3"

S = "${WORKDIR}/git"

do_deploy_append() {
    install -d ${DEPLOY_DIR_IMAGE}
    
    #Creating symlinks
    cd ${DEPLOY_DIR_IMAGE}
    rm -f uImage.bin
    ln -sf ${KERNEL_IMAGE_BASE_NAME}.bin uImage.bin
    
    rm -f kmodules.tgz
    ln -sf ${MODULES_IMAGE_BASE_NAME}.tgz kmodules.tgz
}


# postinst
#

POSTINST_MTD_DEV = "/dev/mtd3"
POSTINST_UIMAGE_FILE = "/boot/uImage-${KERNEL_VERSION}"
POSTINST_FLASH_ERASE = /usr/sbin/flash_erase
POSTINST_NANDWRITE = /usr/sbin/nandwrite

pkg_postinst_kernel-image_append() {
	if [ -z "$D" -a -c "${POSTINST_MTD_DEV}" -a -s "${POSTINST_UIMAGE_FILE}" ]; then
		if [ -x "${POSTINST_FLASH_ERASE}" -a -x "${POSTINST_NANDWRITE}" ]; then
			echo "Reflashing uImage ..."
			${POSTINST_FLASH_ERASE} ${POSTINST_MTD_DEV} 0 0
			${POSTINST_NANDWRITE} -p ${POSTINST_MTD_DEV} ${POSTINST_UIMAGE_FILE}
		else
			echo "flash_erase and nandwrite are required to flash uImage"
		fi
	fi
}
