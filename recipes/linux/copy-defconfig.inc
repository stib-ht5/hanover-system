CONFIG_NAME ?= "tisdk_${MACHINE}_defconfig"

# Copy the configuration file used during the SDK build in Arago to
# the kernel sources to be packaged into the sourceipk.  Then call
# the function to build the sourceipk again.  This is done as a
# do_compile_prepend so that we can pick up any changes to the
# defconfig thay may have been done by the sanitizer code.
do_compile_prepend() {
    #if grep -q '^# Automatically generated make' ${S}/arch/arm/configs/${CONFIG_NAME}; then
    #   cp ${S}/.config ${S}/arch/arm/configs/${CONFIG_NAME}
    if oe_runmake help | grep savedefconfig && oe_runmake savedefconfig; then
        mv ${S}/defconfig ${S}/arch/arm/configs/${CONFIG_NAME}
    else
        cp ${S}/.config ${S}/arch/arm/configs/${CONFIG_NAME}
    fi
    sourceipk_do_create_srcipk
}
