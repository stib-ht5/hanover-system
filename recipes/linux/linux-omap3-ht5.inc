SECTION = "kernel"
DESCRIPTION = "Linux kernel for OMAP3 EVM from PSP, based on linux-omap3 kernel"
LICENSE = "GPLv2"
KERNEL_IMAGETYPE = "uImage"

CONFIG_NAME = "ti8148_ht5_defconfig"

# Used instead of PR revision element because it's how kernel class uses this convention:
MACHINE_KERNEL_PR_append = ".1"
inherit kernel
require copy-defconfig.inc
require setup-defconfig.inc

BRANCH  ?= "branch.dev.HT5"
SRCREV  = "v${PV}"

PR = "r1"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/linux/${BPN}/${KVER}/${BPN}-${PV}.tar.gz;name=v${PV}"

SRC_URI[v2.6.37.6-HDL-HT5-20180731.md5sum] = "9f790338e5fbca6819e2856e39fae992"
SRC_URI[v2.6.37.6-HDL-HT5-20180731.sha256sum] = "9d2540cc331c27e3126ee16fa104ca132a2e8d6f04fa67e948a202781b521009"

# Requires support for SOC_FAMILY matching in COMPATIBLE_MACHINE
COMPATIBLE_MACHINE = "dm814x-ht5"

S = "${WORKDIR}/git"

do_deploy_append() {
    install -d ${DEPLOY_DIR_IMAGE}
    
    #Creating symlinks
    ln -sf ${KERNEL_IMAGE_BASE_NAME}.bin ${DEPLOY_DIR_IMAGE}/uImage.bin
    ln -sf ${MODULES_IMAGE_BASE_NAME}.tgz ${DEPLOY_DIR_IMAGE}/kmodules.tgz
}

POSTINST_MTD_DEV = "/dev/mtd5"
POSTINST_UIMAGE_FILE = "/boot/uImage-${KERNEL_VERSION}"

pkg_postinst_kernel-image_append() {
	if grep -q HT5 /proc/hanover_version; then
		if [ -c ${POSTINST_MTD_DEV} -a -s ${POSTINST_UIMAGE_FILE} ]; then
			echo "Flashing ${POSTINST_UIMAGE_FILE} into ${POSTINST_MTD_DEV}"
			/usr/sbin/flash_erase ${POSTINST_MTD_DEV} 0 0
			/usr/sbin/nandwrite -p ${POSTINST_MTD_DEV} ${POSTINST_UIMAGE_FILE}
		fi
	fi
	true
}
