PR_append = "-hdl1"

SRC_URI += "file://dcron-run.sh"

do_install_append() {
	install -d "${D}/etc/sv/dcron/log"
	install -m 0755 "${WORKDIR}/dcron-run.sh" "${D}/etc/sv/dcron/run"
	ln -snf ../../logrotate/log/run "${D}/etc/sv/dcron/log/run"
}
