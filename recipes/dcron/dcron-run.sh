#!/bin/sh

exec 2>&1

s=/etc/cron.d
c=/var/spool/cron/crontabs

mkdir -p $s $c
exec /usr/sbin/crond -s $s -c $c -t $c -f
