################################################################################
# Hanover SDK target software task definition
#
# Author: Andrei Mironenko <amironenko@hanoverdisplays.com>
#
# Copyright(C) 2012 Hanover Displays Ltd.
# This file is licensed under the terms of the GNU General Public License
# version 2. This program  is licensed "as is" without any warranty of any kind,
# whether express or implied.
################################################################################
require inc/hanover-sdk.inc

PR = "${PRODUCT_RELEASE}"

TOOLCHAIN_HOST_TASK    = "task-hanover-sdk-host"
TOOLCHAIN_TARGET_TASK  = "task-hanover-sdk-target"
