require ${PN}.inc

PR .= ".1"

BRANCH = "master"
SRCREV = "${PV}"

BPV = "${@'.'.join(d.getVar('PV', True).split('.')[0:2])}"

SRC_URI = "http://dev1/source_releases/oe/${BPN}/${BPV}/${BPN}-${PV}.tar.gz;name=v${PV}" 

SRC_URI[v1.3.0.md5sum] = "c3bef4b7cb48582755a863bdab56645f"
SRC_URI[v1.3.0.sha256sum] = "a5ae99cc2c91d99b0aeac3402984808f53593cdf76f91c03dffd89eb7c0f72f7"

S = "${WORKDIR}/git"
