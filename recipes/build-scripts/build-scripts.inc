#Hanover configuration files package

LICENSE = "MIT"

PR = "r0"
PE = "0"

PACKAGES = "${PN}"
PACKAGE_ARCH = "${MACHINE_ARCH}"

do_configure () {
  :
}
 
do_compile () {
  :
}

do_install () {
  :
}

do_deploy () {
    local x= f= mode= common=

    install -d ${DEPLOY_DIR_IMAGE}
    install -d ${DEPLOY_DIR_IMAGE}/scripts
    
    for x in common machine/${MACHINE}; do
        for f in ${S}/$x/*.sh ${S}/$x/*.inc; do
            [ -f "$f" ] || continue

            case "$f" in
            *.sh) mode=0755 ;;
            *)    mode=0644 ;;
            esac
            install -m $mode "$f" "${DEPLOY_DIR_IMAGE}/scripts/"
        done
    done

    package_stagefile_shell ${DEPLOY_DIR_IMAGE}/scripts/*
}


do_deploy[dirs] = "${S}"
addtask deploy before do_package_stage after do_install

INSANE_SKIP_${PN} = True
ALLOW_EMPTY_${PN} = 1
